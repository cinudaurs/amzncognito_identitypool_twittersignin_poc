var TwitterStrategy = require('passport-twitter').Strategy;
var User = require('../models/user');
var auth = require('../config/authorization');
var AWS = require('aws-sdk');

module.exports = function(passport){

    passport.use('twitter', new TwitterStrategy({
            consumerKey     : auth.twitterAuth.consumerKey,
            consumerSecret  : auth.twitterAuth.consumerSecret,
            callbackURL     : auth.twitterAuth.callbackURL
        },
        function(token, tokenSecret, profile, cb) {

            var twitter_cred = token + ";" + tokenSecret;

            // Set the region where your identity pool exists (us-east-1, eu-west-1)
            AWS.config.region = 'us-east-1';

            // Configure the credentials provider to use your identity pool
            AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId: 'us-east-1:6a120e47-37d9-4d70-8704-4295ff575634',
            Logins: { // optional tokens, used for authenticated login
                'api.twitter.com': twitter_cred
                }
        });
    
            // We can set the get method of the Credentials object to retrieve
            // the unique identifier for the end user (identityId) once the provider
            // has refreshed itself
        AWS.config.credentials.get(function(err) {
	    if (err) {
		    console.log("Error: "+err);
		    return;
	    }
	    console.log("Cognito Identity Id: " + AWS.config.credentials.identityId);

	    // Other service clients will automatically use the Cognito Credentials provider
	    // configured in the JavaScript SDK.
	    var cognitoSyncClient = new AWS.CognitoSync();
	    cognitoSyncClient.listDatasets({
		    IdentityId: AWS.config.credentials.identityId,
		    IdentityPoolId: "us-east-1:6a120e47-37d9-4d70-8704-4295ff575634"
	    }, function(err, data) {
		if ( !err ) {
			console.log(JSON.stringify(data));
		}
	});
});  


            User.findOne({ 'twitter.id': profile.id }, function (err, user) {
                if(err){
                    return cb(err)
                } else if (user) {

                    
                    return cb(null, user);
                } else {
                    // if there is no user, create them
                    var newUser                 = new User();

                    // set all of the user data that we need
                    newUser.twitter.id          = profile.id;
                    newUser.twitter.token       = token;
                    newUser.twitter.username    = profile.username;
                    newUser.twitter.displayName = profile.displayName;

                    newUser.save(function(err){
                        if(err){
                            throw err;
                        }else{
                            
                            return cb(null, newUser);
                        }
                    });

                }
            });
        }
    ));

};