var express = require('express');
var router = express.Router();

module.exports = function(passport){
        router.get('/facebook', passport.authenticate('facebook'));

        router.get('/FBcallback',
            passport.authenticate('facebook', { successRedirect: '/dash',
                    failureRedirect: '/login' }));

        router.get('/login/twitter', passport.authenticate('twitter'));    
                    // handle the callback after twitter has authenticated the user       
       // router.get('/twitterCallback', passport.authenticate('twitter', { successRedirect : '/dash', failureRedirect : '/login' }));             
        router.get('/twitter/return', passport.authenticate('twitter', { successRedirect : '/dash', failureRedirect : '/login' }));             
        return router;
}